#SpringAdvertiser
Spring Advertiser - restful api to manage advertisers.

To **build**:
```
./gradlew build
```

For **tests and getting Jacoco report**:
```
./gradlew test jacocoTestReport
```
For **start .jar**:
```
java -jar build/libs/springadvertiser-0.0.1-SNAPSHOT.jar
```

Сlick the link to [see the result](http://127.0.0.1:8080/api/advertiser/all/).