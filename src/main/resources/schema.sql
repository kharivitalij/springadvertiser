create table advertiser
(
   name varchar(255) not null,
   contactName varchar(255) not null,
   creditLimit decimal not null,
   id integer not null AUTO_INCREMENT,
   primary key(id)
);