package com.SpringAdvertiser.springadvertiser.advertiser;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class Advertiser {

    private String name;
    private String contactName;
    private BigDecimal creditLimit;
    private int id;


    public Advertiser(String name, String contactName, BigDecimal creditLimit) {
        this.name = name;
        this.contactName = contactName;
        this.creditLimit = creditLimit;
    }
}
