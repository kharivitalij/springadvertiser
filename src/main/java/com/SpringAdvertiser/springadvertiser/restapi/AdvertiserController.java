package com.SpringAdvertiser.springadvertiser.restapi;

import com.SpringAdvertiser.springadvertiser.advertiser.Advertiser;
import com.SpringAdvertiser.springadvertiser.mapper.AdvertiserMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Synchronized;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("api/advertiser/")
@Api("/api/tasks")
public class AdvertiserController {

    private AdvertiserMapper mapper;

    @GetMapping("/all")
    @ApiOperation(value = "Get all advertisers", response = Advertiser[].class)
    public List<Advertiser> findAll() {
        return mapper.findAll();
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    @ApiOperation(value = "Create a new advertiser")
    public void newAdvertiser(
            @RequestParam(value="name", defaultValue="") String name,
            @RequestParam(value = "contactName", defaultValue = "") String contactName,
            @RequestParam(value="creditLimit", defaultValue = "0") BigDecimal creditLimit
    ) {
        mapper.insert(new Advertiser(name, contactName, creditLimit));
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "Update advertiser")
    public void updateAdvertiser(
            @PathVariable("id") int id,
            @RequestParam(value="name") String name,
            @RequestParam(value = "contactName") String contactName,
            @RequestParam(value="creditLimit") BigDecimal creditLimit
    ) {
        mapper.update(id, name, contactName, creditLimit);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete advertiser")
    public void deleteAdvertiser(@PathVariable("id") int id) {
        mapper.delete(id);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Get advertiser")
    public Advertiser getAdvertiser(@PathVariable("id") int id) {
        return mapper.get(id);
    }

    @RequestMapping(value = "/check/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Validate if the advertiser has enough credit to perform a\n" +
            "transaction")
    public boolean checkAdvertiser(
            @PathVariable("id") int id,
            @RequestParam("price") BigDecimal price) {

        Advertiser advertiser = mapper.get(id);
        int comparison = advertiser.getCreditLimit().compareTo(price);
        return comparison > 0 || comparison == 0;
    }

    //@Synchronized
    @RequestMapping(value = "/deduct/{id}", method = RequestMethod.POST)
    @ApiOperation(value = "Deduct credits from the advertiser’s account if there are enough funds.")
    public boolean deductCredits(
            @PathVariable("id") int id,
            @RequestParam("count") BigDecimal count) {

        synchronized (this) {
            Advertiser advertiser = mapper.get(id);
            int comparison = advertiser.getCreditLimit().compareTo(count);
            if (comparison > 0) {
                mapper.update(id, advertiser.getName(), advertiser.getContactName(),
                        advertiser.getCreditLimit().subtract(count));
                return true;
            }
            return false;
        }
    }

    public AdvertiserController(AdvertiserMapper mapper) {
        this.mapper = mapper;
    }
}
