package com.SpringAdvertiser.springadvertiser.mapper;

import com.SpringAdvertiser.springadvertiser.advertiser.Advertiser;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Mapper
@Repository
public interface AdvertiserMapper {

    @Select("select * from advertiser")
    List<Advertiser> findAll();

    @Insert("insert into advertiser(name, contactName, creditLimit) " +
            "values(#{name}, #{contactName}, #{creditLimit})")
    void insert(Advertiser advertiser);

    @Update("update advertiser set name=#{name}, contactName=#{contactName}, " +
            "creditLimit=#{creditLimit} where id=#{id}")
    void update(int id, String name, String contactName, BigDecimal creditLimit);

    @Delete("delete from advertiser where id=#{id}")
    void delete(int id);

    @Select("select * from advertiser where id=#{id}")
    Advertiser get(int id);

}
