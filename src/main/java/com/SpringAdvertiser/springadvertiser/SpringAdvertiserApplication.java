package com.SpringAdvertiser.springadvertiser;

import com.SpringAdvertiser.springadvertiser.advertiser.Advertiser;
import org.apache.ibatis.type.MappedTypes;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MappedTypes(Advertiser.class)
@MapperScan("com.SpringAdvertiser.springadvertiser.mapper")
@SpringBootApplication
public class SpringAdvertiserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringAdvertiserApplication.class, args);
	}

}
