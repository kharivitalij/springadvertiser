package com.SpringAdvertiser.springadvertiser.restapi;

import static io.restassured.RestAssured.get;
import static org.hamcrest.CoreMatchers.equalTo;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


public class AdvertiserControllerTest {

    @BeforeClass
    public static void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 8080;
    }

    @Test
    public void testNewAdvertiser() {
        RequestSpecification request = RestAssured.given();

        request.post("/api/advertiser/new?name=tester&creditLimit=30");

        get("/api/advertiser/get/2")
                .then()
                .body("id", equalTo(2))
                .body("name", equalTo("tester"))
                .body("contactName", equalTo(""))
                .body("creditLimit", equalTo(30));

    }

    @Test
    public void testDeleteAdvertiser() {
        RequestSpecification request = RestAssured.given();

        request.delete("/api/advertiser/delete/1");

        get("/api/advertiser/get/1")
                .then()
                .statusCode(200);
    }

    @Test
    public void testGet() {
        get("/api/advertiser/get/1")
                .then()
                .body("id", equalTo(1))
                .body("name", equalTo("Test"))
                .body("contactName", equalTo("Advertiser"))
                .body("creditLimit", equalTo(100));
    }

    @Test
    public void testCheck() {
        Response response = get("/api/advertiser/check/1/?price=120");
        ResponseBody body = response.getBody();
        Assert.assertEquals("false", body.asString());
    }

    @Test
    public void testDeductAdvertiser() {
        RequestSpecification request = RestAssured.given();

        request.post("/api/advertiser/deduct/2?count=20");

        get("/api/advertiser/get/2")
                .then()
                .body("id", equalTo(2))
                .body("name", equalTo("tester"))
                .body("contactName", equalTo(""))
                .body("creditLimit", equalTo(10));

    }
}
