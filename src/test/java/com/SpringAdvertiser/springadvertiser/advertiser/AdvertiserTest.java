package com.SpringAdvertiser.springadvertiser.advertiser;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AdvertiserTest {

    private Advertiser advertiser;

    @Before
    public void setUp() {
        this.advertiser = new Advertiser("tester",
                "testerContactName", 100);
    }

    @Test
    public void getNameTest() {
        Assert.assertEquals(this.advertiser.getName(), "tester");
    }

    @Test
    public void setNameTest() {
        this.advertiser.setName("theSecondName");
        Assert.assertEquals(this.advertiser.getName(), "theSecondName");
    }

    @Test
    public void getContactNameTest() {
        Assert.assertEquals(this.advertiser.getContactName(), "testerContactName");
    }

    @Test
    public void setContactNameTest() {
        this.advertiser.setContactName("theSecondContactName");
        Assert.assertEquals(this.advertiser.getContactName(), "theSecondContactName");
    }

    @Test
    public void getCreditLimitTest() {
        Assert.assertEquals(this.advertiser.getCreditLimit(), 100);
    }

    @Test
    public void setCreditLimitTest() {
        this.advertiser.setCreditLimit(110);
        Assert.assertEquals(this.advertiser.getCreditLimit(), 110);
    }

    @Test
    public void getId() {
        Assert.assertEquals(this.advertiser.getId(), 0);
    }

    @Test
    public void setId() {
        this.advertiser.setId(1);
        Assert.assertEquals(this.advertiser.getId(), 1);
    }

    @Test
    public void Advertiser() {
        Advertiser advertiser = new Advertiser();
        Assert.assertEquals(advertiser.getId(), 0);
        Assert.assertNull(advertiser.getName());
        Assert.assertNull(advertiser.getContactName());
        Assert.assertEquals(advertiser.getCreditLimit(), 0);
    }

    @After
    public void tearDown() {
        this.advertiser = null;
    }
}
